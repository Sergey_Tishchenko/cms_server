import * as mongoose from "mongoose";
import {Model} from "mongoose";

import {Dao} from "../dal/Dao";
import {TagModel} from "../dal/schema/tag.schema";
import {PostModel} from "../dal/schema/post.schema";
import {RoleModel} from "../dal/schema/role.schema";
import {UserModel} from "../dal/schema/user.schema";
import {tagsData} from "./data/tags.data";
import {postData} from "./data/post.data";
import {roleData} from "./data/role.data";
import {userData} from "./data/user.data";

Dao.connect().then(() => {
    return Promise.all([
        asyncRemove(TagModel),
        asyncRemove(PostModel),
        asyncRemove(RoleModel),
        asyncRemove(UserModel),
    ])
}).then(() => {
    return Promise.all([
        new RoleModel(roleData[0]).save(),
        new RoleModel(roleData[1]).save(),
        new RoleModel(roleData[2]).save(),
    ]);
}).then(() => {
    return Promise.all([
        new UserModel(userData[0]).save(),
        new UserModel(userData[1]).save(),
        new UserModel(userData[2]).save(),
    ]);
}).then(() => {
    return Promise.all([
        new TagModel(tagsData[0]).save(),
        new TagModel(tagsData[1]).save(),
        new TagModel(tagsData[2]).save(),
        new TagModel(tagsData[3]).save(),
        new TagModel(tagsData[4]).save(),
        new TagModel(tagsData[5]).save(),
        new TagModel(tagsData[6]).save()
    ])
}).then(res=> {
    if (res) {
        return Promise.all([
            new PostModel(postData[0]).save(),
            new PostModel(postData[1]).save(),
            new PostModel(postData[2]).save(),
            new PostModel(postData[3]).save(),
            new PostModel(postData[4]).save(),
            new PostModel(postData[5]).save(),
            new PostModel(postData[6]).save(),
            new PostModel(postData[7]).save(),
            new PostModel(postData[8]).save(),
            new PostModel(postData[9]).save(),
            new PostModel(postData[10]).save(),
            new PostModel(postData[11]).save(),
            new PostModel(postData[12]).save(),
            new PostModel(postData[13]).save(),
            new PostModel(postData[14]).save(),
            new PostModel(postData[15]).save(),
            new PostModel(postData[16]).save(),
            new PostModel(postData[17]).save()
        ]);
    }
}).then(() => {
    mongoose.disconnect(() => {
        console.log('\x1b[33m', 'database filled', '\x1b[0m');
        console.log('\x1b[33m', 'disconnected from database', '\x1b[0m');})
}).catch(err => {
    console.log(err);
    mongoose.disconnect(() => console.log('disconnected from database'));
});




function asyncRemove(model: Model<any>) {
    return new Promise<null>((resolve, reject) => {
        model.remove((err) => {
            if (err) {reject(err);}
            else {resolve()};
        })
    })
};