import {Tag} from '../../model/Tag';

export const tagsData = [
    new Tag('000000000000000000000001', 'Business'),
    new Tag('000000000000000000000002', 'Sport'),
    new Tag('000000000000000000000003', 'Technology'),
    new Tag('000000000000000000000004', 'Earth'),
    new Tag('000000000000000000000005', 'Future'),
    new Tag('000000000000000000000006', 'Travel'),
    new Tag('000000000000000000000007', 'Autos'),
];