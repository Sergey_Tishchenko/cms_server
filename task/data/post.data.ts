import {Post} from "../../model/Post";
import {postTitle} from "./post.title.data";
import {postContent} from "./post.content.data";
import {postDesc} from "./post.description.data";

let post1 = new Post(null, postTitle[0], new Date('2017-6-21'), '/img/large_1.jpg', '/img/small_1.jpg', postDesc[0], postContent[0], 0, [], '000000000000000000000001');
let post2 = new Post(null, postTitle[1], new Date('2017-6-9'), '/img/large_2.jpg', '/img/small_2.jpg', postDesc[1], postContent[1], 1, [], '000000000000000000000002');
let post3 = new Post(null, postTitle[2], new Date('2017-6-2'), '/img/large_3.jpg', '/img/small_3.jpg', postDesc[2], postContent[2], 2, [], '000000000000000000000003');
let post4 = new Post(null, postTitle[3], new Date('2017-5-25'), '/img/large_4.jpg', '/img/small_4.jpg', postDesc[3], postContent[3], -1, [], '000000000000000000000001');
let post5 = new Post(null, postTitle[4], new Date('2017-5-2'), '/img/large_5.jpg', '/img/small_5.jpg', postDesc[4], postContent[4], -1, [], '000000000000000000000002');
let post6 = new Post(null, postTitle[5], new Date('2017-4-23'), '/img/large_6.jpg', '/img/small_6.jpg', postDesc[5], postContent[5], -1, [], '000000000000000000000003');
let post7 = new Post(null, postTitle[6], new Date('2017-3-28'), '/img/large_7.jpg', '/img/small_7.jpg', postDesc[6], postContent[6], -1, [], '000000000000000000000001');
let post8 = new Post(null, postTitle[7], new Date('2017-3-19'), '/img/large_8.jpg', '/img/small_8.jpg', postDesc[7], postContent[7], -1, [], '000000000000000000000002');
let post9 = new Post(null, postTitle[8], new Date('2017-3-2'), '/img/large_9.jpg', '/img/small_9.jpg', postDesc[8], postContent[8], -1, [], '000000000000000000000003');
let post10 = new Post(null, postTitle[9], new Date('2017-1-25'), '/img/large_10.jpg', '/img/small_10.jpg', postDesc[9], postContent[9], -1, [], '000000000000000000000001');
let post11 = new Post(null, postTitle[10], new Date('2017-1-25'), '/img/large_11.jpg', '/img/small_11.jpg', postDesc[10], postContent[10], -1, [], '000000000000000000000002');
let post12 = new Post(null, postTitle[11], new Date('2017-1-22'), '/img/large_12.jpg', '/img/small_12.jpg', postDesc[11], postContent[11], -1, [], '000000000000000000000003');
let post13 = new Post(null, postTitle[12], new Date('2017-1-15'), '/img/large_13.jpg', '/img/small_13.jpg', postDesc[12], postContent[12], -1, [], '000000000000000000000001');
let post14 = new Post(null, postTitle[13], new Date('2016-11-25'), '/img/large_14.jpg', '/img/small_14.jpg', postDesc[13], postContent[13], -1, [], '000000000000000000000002');
let post15 = new Post(null, postTitle[14], new Date('2016-11-5'), '/img/large_15.jpg', '/img/small_15.jpg', postDesc[14], postContent[14], -1, [], '000000000000000000000003');
let post16 = new Post(null, postTitle[15], new Date('2016-9-28'), '/img/large_16.jpg', '/img/small_16.jpg', postDesc[15], postContent[15], -1, [], '000000000000000000000001');
let post17 = new Post(null, postTitle[16], new Date('2016-9-10'), '/img/large_17.jpg', '/img/small_17.jpg', postDesc[16], postContent[16], -1, [], '000000000000000000000002');
let post18 = new Post(null, postTitle[17], new Date('2016-9-4'), '/img/large_18.jpg', '/img/small_18.jpg', postDesc[17], postContent[17], -1, [], '000000000000000000000003');

let t1 = '000000000000000000000001';
let t2 = '000000000000000000000002';
let t3 = '000000000000000000000003';
let t4 = '000000000000000000000004';
let t5 = '000000000000000000000005';
let t6 = '000000000000000000000006';
let t7 = '000000000000000000000007';

post1.tags.push(t5);
post2.tags.push(t4);
post3.tags.push(t2);
post4.tags.push(t4, t6);
post5.tags.push(t6);
post6.tags.push(t1);
post7.tags.push(t7);
post8.tags.push(t2, t2);
post9.tags.push(t3);
post10.tags.push(t6);
post11.tags.push(t4);
post12.tags.push(t4);
post13.tags.push(t3, t4);
post14.tags.push(t4);
post15.tags.push(t2, t7, t3);
post16.tags.push(t4, t6);
post17.tags.push(t4, t6);
post18.tags.push(t5, t3);







export const postData =[
  post1,
  post2,
  post3,
  post4,
  post5,
  post6,
  post7,
  post8,
  post9,
  post10,
  post11,
  post12,
  post13,
  post14,
  post15,
  post16,
  post17,
  post18,
];