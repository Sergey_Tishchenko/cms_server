import {Role} from "../../model/Role";
export const roleData = [
    new Role('000000000000000000000001', 'admin'),
    new Role('000000000000000000000002', 'editor'),
    new Role('000000000000000000000003', 'author'),
];