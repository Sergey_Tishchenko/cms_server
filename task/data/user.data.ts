import {User} from "../../model/User";

export const userData = [
    new User('000000000000000000000001', 'Admin_First', 'Admin_Last', 'admin', 'admin@gmail.com', 'admin', '000000000000000000000001'),
    new User('000000000000000000000002', 'Editor_First', 'Editor_Last', 'editor', 'editor@gmail.com', 'editor', '000000000000000000000002'),
    new User('000000000000000000000003', 'Author_First', 'Author_Last', 'author', 'author@gmail.com', 'author', '000000000000000000000003')
];