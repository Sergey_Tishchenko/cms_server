import {Tag} from "./Tag";

export class Post {
    constructor(
        public id: string,
        public title: string,
        public date: Date,
        public imgSrc: string,
        public iconSrc: string,
        public description: string,
        public content: string,
        public order: number,
        public tags: string[],
        public user: string
    ){}
}