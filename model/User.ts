export class User {
    constructor(
        public _id: string,
        public firstName: string,
        public lastName: string,
        public login: string,
        public email: string,
        public pass: string,
        public role: string
    ) {}
}
