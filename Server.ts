import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';

import MainRouter from './routes/MainRouter';
import TagRouter from './routes/api/v1/TagRouter';
import PostRouter from './routes/api/v1/PostRouter';
import RoleRouter from './routes/api/v1/RoleRouter';
import UserRouter from './routes/api/v1/UserRouter';
import { configServer } from './config/server.config';
import {authenticate} from "./middleware/Authorization";

/**
 * Main class for configuring server.
 */
export class Server {
    express: express.Application;
    servicesPath: string = configServer.apiPath;

    constructor() {
        this.express = express();
        this.settings();
        this.middleWare();
        this.routes();
        this.errors();
    }

    private settings(): void {
        this.express.set('view engine', 'ejs');
        this.express.set('views', __dirname + '/views');

        this.express.use(cors());
        this.express.use(express.static(__dirname + '/public'))

        this.express.use(bodyParser.json());
    }

    private middleWare(): void {
        // this.express.use(this.servicesPath, authenticate);
    }

    private routes(): void {
        this.express.use(MainRouter);
        this.express.use(this.servicesPath, TagRouter);
        this.express.use(this.servicesPath, PostRouter);
        this.express.use(this.servicesPath, RoleRouter);
        this.express.use(this.servicesPath, UserRouter);
    }

    private errors(): void {
        this.express.use((req, res) => {
            res.type('text/plain');
            res.status(404);
            res.send('404 - Not Found');
        });

        this.express.use((err, req, res, next) => {
            console.error(err.stack);
            res.type('text/plain');
            res.status(500);
            res.send('500 - Server Error');
        })
    }

    public listen(port: number): void {
        this.express.listen(port, () => console.log('\x1b[33m', `Server is running on port: ${port}`, '\x1b[0m'))
    }
}