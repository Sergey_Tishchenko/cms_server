import { Server } from './Server';
import { configServer } from './config/server.config'
import {Dao} from "./dal/Dao";

Dao.connect().then(() => new Server().listen(configServer.port));