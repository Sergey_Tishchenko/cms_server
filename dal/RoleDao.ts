import {RoleModel} from "./schema/role.schema";

export class RoleDao {
    static getAll(req, res, next): void {
        console.log('RoleDao getAll');
        RoleModel.find({}, (err, data) => sendJson(res, err, data));
    }

    static getById(req, res, next) {
        console.log('RoleDao getById: ' + req.params['id']);
        RoleModel.findById(req.params['id'], (err, data) => sendJson(res, err, data));
    };
}

function sendJson(res, err, data) {
    if (err) {
        console.log(err);
        res.statusCode = 400;
        res.json(err);
    }
    else {
        // console.log(data);
        res.json(data);
    }
}
