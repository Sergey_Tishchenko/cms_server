import {UserModel} from "./schema/user.schema";
import * as jwt from 'jsonwebtoken';
import {configServer} from "../config/server.config";
import {Document} from 'mongoose';

export class UserDao {
    static getAll(req, res, next): void {
        console.log('UserDao getAll');
        UserModel.find({})
            .populate('role')
            .exec((err, data) => sendJson(res, err, data));
    }

    static getById(req, res, next) {
        console.log('UserDao getById: ' + req.params['id']);
        UserModel.findById(req.params['id'])
            .populate('role')
            .exec((err, data) => sendJson(res, err, data));
    };

    static create(req, res, next) {
        console.log('UserDao create');
        new UserModel(req.body)
            .save((err, data) => {
                console.log(err);
                console.log(data);
                if(err) {
                    // console.dir(err);
                    if (err.errmsg.includes('duplicate key')) {
                        err = {msg: 'user with this login already exists'};
                    } else {
                        err = {msg: err.json().errmsg};
                    }
                };
                sendJson(res, err, data);
            }
        );
    };

    static update(req, res, next) {
        console.log('UserDao update: ' + req.params['id']);
        console.log(req.body);
        UserModel.findByIdAndUpdate(req.params['id'], {$set: req.body}, {new: true})
            .exec((err, data) => {
            console.log(err);
            console.log(data);
                if(err) {
                    // console.dir(err);
                    if (err.errmsg.includes('duplicate key')) {
                        err = {msg: 'user with this login already exists'};
                    } else {
                        err = {msg: err.json().errmsg};
                    }
                };
                sendJson(res, err, data);
            });
    };

    static delete(req, res, next) {
        console.log('UserDao delete');
        UserModel.findByIdAndRemove(req.params['id'], (err, data) => sendJson(res, err, data))
    };

    static authenticate(req, res, next) {
        console.log('UserDao authenticate');
        let login = req.body.login;
        let pass = req.body.pass;
        // console.log(login + ' -> ' + pass);
        UserModel.findOne({login: login})
            .populate('role')
            .exec((err, user) => {
            // console.log(data);
            if (err) {
                    res.statusCode = 400;
                    res.json(err);
                } else {
                    if (!user) {
                        res.statusCode = 404;
                        res.json({msg: 'user not found'});
                    } else {
                        if (user['pass'] !== pass) {
                            res.statusCode = 400;
                            res.json({msg: 'incorrect password'});
                        } else {
                            console.log('user authenticated');
                            let token = UserDao.generateJWT(user);
                            res.json({token: token, user: user} );
                        }
                    }
                }
            });
    };

    static generateJWT(user: Document): string {
        let claims = {
            sub: user['_id'],
            iss: 'CMSserver',
            permissions: user['role'].name
        };

        let token = jwt.sign(claims, configServer.secretKey, {expiresIn: configServer.expToken});
        console.dir(token);
        console.log(jwt.decode(token, ));
        return token;
    }
}


function sendJson(res, err, data) {
    if (err) {
        console.log(err);
        res.statusCode = 400;
        res.json(err);
    }
    else {
        // console.log(data);
        res.json(data);
    }
}