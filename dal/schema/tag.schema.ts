import * as mongoose from "mongoose";
import {Tag} from "../../model/Tag";

let tagSchema = new mongoose.Schema({
    name: {type: String, unique: true}
}, {
    toJSON: {
        transform: (doc, ret) => {
            // ret.id = ret._id;
            delete ret.__v;
            // delete ret._id;
        }
    },
    toObject: {
        transform: (doc, ret) => {
            // ret.id = ret._id;
            delete ret.__v;
            // delete ret._id;
        }
    }
});

export const TagModel = mongoose.model('Tag', tagSchema);
