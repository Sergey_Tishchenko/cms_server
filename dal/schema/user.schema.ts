import * as mongoose from "mongoose";

let userSchema = new mongoose.Schema({
        firstName: String,
        lastName: String,
        login: {type: String, unique: true},
        email: String,
        pass: String,
        role: {type: mongoose.Schema.Types.ObjectId, ref: 'Role'}
    },
    {
        toJSON: {
            transform: (doc, ret) => {
                // ret.id = ret._id;
                delete ret.__v;
                // delete ret._id;
            }
        },
        toObject: {
            transform: (doc, ret) => {
                // ret.id = ret._id;
                delete ret.__v;
                // delete ret._id;
            }
        }
    }
);

export const UserModel = mongoose.model('User', userSchema);