import * as mongoose from "mongoose";

let postSchema = new mongoose.Schema({
    title: String,
    date: Date,
    imgSrc: String,
    iconSrc: String,
    description: String,
    content: String,
    tags: [{type: mongoose.Schema.Types.ObjectId, ref: 'Tag'}],
    order: Number,
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
},{
    toJSON: {
        transform: (doc, ret) => {
            // ret.id = ret._id;
            delete ret.__v;
            // delete ret._id;
        }
    },
    toObject: {
        transform: (doc, ret) => {
            // ret.id = ret._id;
            delete ret.__v;
            // delete ret._id;
        }
    }
});

export const PostModel = mongoose.model('Post', postSchema);