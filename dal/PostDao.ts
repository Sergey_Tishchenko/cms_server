import {PostModel} from "./schema/post.schema";

export class PostDao {
    static getAllPost(req, res, next): void {
        console.log('PostDao getAllPost');
        PostModel.find({}).populate('tags').populate('user')
            .exec((err, data) => {sendJson(res, err, data)});
    }

    static getPostById(req, res, next): void {
        console.log('PostDao getPostById:' + req.params['id']);
        PostModel.findById(req.params['id']).populate('tags').populate('user')
            .exec((err, data) => {sendJson(res, err, data)});
    }



    //partial
    static getAllPostInfo(req, res) {
        let lim = Number(req.query['lim']);
        console.log(`PostDao getAllPostInfo, lim=${lim}`);
        if (!lim) lim =0;
        PostModel.find({},'id title iconSrc')
            .limit(lim)
            .exec( (err, data) => {sendJson(res, err, data)});
    }

    static getPostDetails(req, res) {
        let key = req.query['key'];
        let val = req.query['val'];
        console.log(`PostDao getPostDetails: key=${key}, val=${val}`);
        switch (key) {
            case 'tag': {
                PostModel.find(
                    {'tags': req.query['val']},
                'id title date imgSrc description',
                    (err, data) => {sendJson(res, err, data)}
                );
            }; break;

            case 'home': {
                PostModel.find({'order': {$gt: -1}}, 'id title date imgSrc description')
                    .sort({'order': 1})
                    .exec((err, data) => {sendJson(res, err, data)});
            }; break;
            case 'archive': {
                if (val) {
                    let date = new Date(+val);
                    // console.log(date);
                    let searchDate: string = date.toISOString().slice(0, 7);
                    console.log(searchDate);
                    PostModel.find(
                        {$where : `this.date.toJSON().slice(0, 7) == "${searchDate}"` }
                    )
                        .sort({'date': -1})
                        .exec((err, data) => {
                            sendJson(res, err, data)
                        });

                } else {
                    PostModel.find({'order': {$lt: 0}}, 'id title date imgSrc description')
                        .sort({'date': -1})
                        .exec((err, data) => {
                        console.log(data);
                            sendJson(res, err, data)
                        });
                }
            }; break;

            default: {
                PostModel.find({},'id title date imgSrc iconSrc description user')
                    .populate('user')
                    .exec((err, data) => {sendJson(res, err, data)});}
        }
    }

    static getAllDate(req, res) {
        let lim = Number(req.query['lim']);
        if (!lim) lim =0;
        console.log(`PostDao getAllDate, lim=${lim}`);
        PostModel.find({}, 'date').sort({'date': -1})
            .limit(lim)
            .exec((err, data) => {sendJson(res, err, data)});
    }

    static getUniqueYearMonth(req, res) {
        let lim = Number(req.query['lim']);
        if (!lim) lim =0;
        console.log(`PostDao getUniqueYearMonth, lim=${lim}`);
        PostModel.aggregate([{$group: {_id: {date: {$dateToString: {format: "%Y-%m", date: "$date"}}}}}])
            .sort({'_id.date': -1})
            .limit(lim)
            .exec((err, data) => {
            // console.log(data);
                const list = [];
            if(!err) {
                data.forEach(item => {list.push( {'date': (item['_id'].date)} )})
            }
            sendJson(res, err, list)});
    }
}




function sendJson(res, err, data) {
    if (err) {
        console.log(err);
        res.end(err);
    }
    else {
        // console.log(data);
        res.json(data);
    }
}