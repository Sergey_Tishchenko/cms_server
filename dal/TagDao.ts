import {TagModel} from "./schema/tag.schema";
import {PostModel} from "./schema/post.schema";

export class TagDao {
    static getAll(req, res, next): void {
        console.log('TagDao getAll');
        TagModel.find({},(err, data) => sendJson(res, err, data));
    }

    static getById(req, res, next) {
        console.log('TagDao getById: ' + req.params['id']);
        TagModel.findById(req.params['id'], (err, data) => sendJson(res, err, data));
    };

    static create(req, res, next) {
        console.log('TagDao create');
        new TagModel(req.body).save((err, data) => sendJson(res, err, data));
    };

    static update(req, res, next) {
        console.log('TagDao update');
        TagModel.findByIdAndUpdate(req.params['id'], {$set: req.body}, {new: true}, (err, data) => sendJson(res, err, data))
    };

    static delete(req, res, next) {
        console.log('TagDao delete');
        let id = req.params['id'];
        PostModel.findOne({'tags': id})
            .exec((err, data) => {
            if (err) {
                (err, data) => sendJson(res, err, data);
            } else {
                if (data) {
                    sendJson(res, {msg: 'tag is in use'}, null);
                } else {
                    TagModel.findByIdAndRemove(id, (err, data) => sendJson(res, err, data));
                }
            }
            });

    };
}


function sendJson(res, err, data) {
    if (err) {
        console.log(err);
        res.statusCode = 400;
        res.json(err);
    }
    else {
        // console.log(data);
        res.json(data);
    }
}