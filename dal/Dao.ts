import * as mongoose from 'mongoose';
import { configDb } from '../config/db.config'

export class Dao {

    static connect(): Promise<null> {
        return new Promise((resolve, reject) => {
            mongoose.connect(configDb.uri, (err) => {
                if(err) {
                    console.log(err);
                    reject(err);
                }
                else {
                    console.log('\x1b[33m', 'connected to Database', '\x1b[0m');
                    resolve();
                }
            });
        });



    }
}