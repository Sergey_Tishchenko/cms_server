import * as jwt from 'jsonwebtoken';
import {configServer} from "../config/server.config";
import {Request, Response} from 'express';

export function authenticate(req: Request, res: Response, next): void {
    let token = req.header('Authorization');
    if (token) {
        jwt.verify(token, configServer.secretKey, (err, decoded) => {
            if (err) {
                res.statusCode = 401;
                res.json(err);
                console.log(err);
            } else {
                next();
            }
        })
    } else {
        res.statusCode = 401;
        res.json({msg: 'authorization required'});
        console.log('authorization required');
    }
}