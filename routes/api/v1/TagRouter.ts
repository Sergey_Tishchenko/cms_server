import * as express from 'express';
import {TagDao} from "../../../dal/TagDao";

class TagRouter {
    router: express.Router;

    constructor() {
        this.router = express.Router();
        this.init();
    }

    private init(): void {
        this.router.get('/tags', TagDao.getAll);
        this.router.get('/tags/:id', TagDao.getById);
        this.router.post('/tags', TagDao.create);
        this.router.put('/tags/:id', TagDao.update);
        this.router.delete('/tags/:id', TagDao.delete);
    }
}

export default new TagRouter().router;