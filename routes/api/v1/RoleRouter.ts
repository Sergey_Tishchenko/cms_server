import * as express from 'express';
import {RoleDao} from "../../../dal/RoleDao";

class RoleRouter {
    router: express.Router;

    constructor() {
        this.router = express.Router();
        this.init();
    }

    private init(): void {
        this.router.get('/roles', RoleDao.getAll);
        this.router.get('/roles/:id', RoleDao.getById);
    }
}

export default new RoleRouter().router;