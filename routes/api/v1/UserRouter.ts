import * as express from 'express';
import {UserDao} from "../../../dal/UserDao";

class UserRouter {
    router: express.Router;

    constructor() {
        this.router = express.Router();
        this.init();
    }

    private init(): void {
        this.router.get('/users', UserDao.getAll);
        this.router.get('/users/:id', UserDao.getById);
        this.router.post('/users', UserDao.create);
        this.router.put('/users/:id', UserDao.update);
        this.router.delete('/users/:id', UserDao.delete);
        this.router.post('/authenticate', UserDao.authenticate);
    }
}

export default new UserRouter().router;