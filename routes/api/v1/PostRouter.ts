import * as express from 'express';
import { PostDao } from "../../../dal/PostDao"

class PostRouter {
    router: express.Router;

    constructor() {
        this.router = express.Router();
        this.init();
    }

    private init(): void {
        this.router.get('/posts', PostDao.getAllPost);
        this.router.get('/posts/:id', PostDao.getPostById);

        this.router.get('/postinfos', PostDao.getAllPostInfo);
        this.router.get('/postdetails', PostDao.getPostDetails);

        // this.router.get('/dates', PostDao.getAllDate);
        this.router.get('/dates', PostDao.getUniqueYearMonth);
    }
}

export default new PostRouter().router;