import * as express from 'express';

class MainRouter {
    router: express.Router;

    constructor() {
        this.router = express.Router();
        this.init();
    }

    private init(): void {
        this.router.get('/', this.main);
        this.router.get('/page1', this.page1);
    }

    private main(req, res): void {
        res.end('<h1>Welcome to Node.js</h1>');
    }

    private page1(req, res): void {
        res.render('page1');
    }
}

export default new MainRouter().router;